import Should from 'should'
import convert from '../convert'
import sort from '../sort'
import search from '../search'

describe('version convertion', () => {
    it('should convert an object of versions to list of strings', () => {
        //Arrange
        const versionObjects = [
            {
                majorVersion: 2,
                minorVersion: 1
            },
            {
                majorVersion: 2,
                minorVersion: 10
            },
            {
                majorVersion: 1,
                minorVersion: null
            },
            {
                majorVersion: 0,
                minorVersion: 2
            },
            {
                majorVersion: 0,
                minorVersion: 1
            },
            {
                majorVersion: 0,
                minorVersion: 0
            },
            {
                majorVersion: 0,
                minorVersion: null
            }
        ]

        //Act
        var result = convert.toStrings(versionObjects)

        //Assert
        result.should.have.length(7)
        result[0].should.equal("2.1.0")
        result[6].should.equal("0.0.0")

    })

    it('sort stringified versions', () => {
        //Arrange because fuck DRY principles
        const versionObjects = [
            {
                majorVersion: 2,
                minorVersion: 1
            },
            {
                majorVersion: 2,
                minorVersion: 10
            },
            {
                majorVersion: 1,
                minorVersion: null
            },
            {
                majorVersion: 0,
                minorVersion: 2
            },
            {
                majorVersion: 0,
                minorVersion: 1
            },
            {
                majorVersion: 0,
                minorVersion: 0
            },
            {
                majorVersion: 0,
                minorVersion: null
            }
        ]

        //Act
        var versions = convert.toStrings(versionObjects)
        var result = sort.versionStrings(versions)
        
        //Assert
        result.should.have.length(6)
        result[0].should.equal("2.10.0")
        result[1].should.equal("2.1.0")

    })

    it('should return previous version', () => {
        //Arrange
        const versionObjects = [
            {
                majorVersion: 2,
                minorVersion: 1
            },
            {
                majorVersion: 2,
                minorVersion: 10
            },
            {
                majorVersion: 1,
                minorVersion: null
            },
            {
                majorVersion: 0,
                minorVersion: 2
            },
            {
                majorVersion: 0,
                minorVersion: 1
            },
            {
                majorVersion: 0,
                minorVersion: 0
            },
            {
                majorVersion: 0,
                minorVersion: null
            }
        ]

        //Act
        var versions = convert.toStrings(versionObjects)
        var result = search.previousVersion(versions, "2.9.1")
        
        result.should.equal("2.1.0")

    })

     it('should return lowest version if no previous version', () => {
        //Arrange
        const versionObjects = [
            {
                majorVersion: 2,
                minorVersion: 1
            },
            {
                majorVersion: 2,
                minorVersion: 10
            },
            {
                majorVersion: 1,
                minorVersion: null
            },
            {
                majorVersion: 0,
                minorVersion: 2
            },
            {
                majorVersion: 0,
                minorVersion: 1
            },
            {
                majorVersion: 0,
                minorVersion: 0
            },
            {
                majorVersion: 0,
                minorVersion: null
            }
        ]

        //Act
        var versions = convert.toStrings(versionObjects)
        var result = search.previousVersion(versions, "0.0.0")
        
        result.should.equal("0.0.0")

    })
})