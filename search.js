module.exports = {
    previousVersion: function(versions, currentVersion) {
        const sort = require('./sort')
        
        versions.push(currentVersion)
        versions = sort.versionStrings(versions)
        
        const index = versions.indexOf(currentVersion)

        return versions[index + 1] || versions.pop()
    }
}