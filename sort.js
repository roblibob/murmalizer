module.exports = {
    /**
     * Sorts versions represented in an array of strings
     */
    versionStrings: function(versions) {
        var convert = require('./convert');
        
        //Convert to objects for easy sorting
        var objectVersions = convert.toObjects(versions)

        //Sort by major, minor, patch
        objectVersions.sort((a, b) => {
            if (a.majorVersion < b.majorVersion) {
                return 1;
            }
            if (a.majorVersion > b.majorVersion) {
                return -1;
            }
            if (a.minorVersion < b.minorVersion) {
                return 1;
            }
            if (a.minorVersion > b.minorVersion) {
                return -1;
            }
            if (a.patchVersion < b.patchVersion) {
                return 1;
            }
            if (a.patchVersion > b.patchVersion) {
                return -1;
            }
        })

        //Convert back to strings
        var result = convert.toStrings(objectVersions)
        //Remove duplicates
        .filter((item, index, versions) => {
            return !index || item != versions[index - 1]
        });

        return result
    }
}