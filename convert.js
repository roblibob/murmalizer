module.exports = {
    /**
     * Convert an object with versions to array with string representations of versions
     */
    toStrings: function(versions) {
        return versions.map((version) => { 
            return [
                version.majorVersion || 0,
                version.minorVersion || 0,
                version.patchVersion || 0
            ].join(".")
        })
    },

    toObjects: function(versions) {
        //Split each version string on dot and parse values to int and return as object
        var result = versions.map((version) => { 
            var versarr = version.split(".")
            return {
                "majorVersion": parseInt(versarr[0]),
                "minorVersion": parseInt(versarr[1]),
                "patchVersion": parseInt(versarr[2])
            }
        })
        return result
    }
}
